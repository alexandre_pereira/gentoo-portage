#! /bin/bash

for x in `quse -C -D $1 | cut -d":" -f3` ;  
	do
		echo "checking $x";
		if eix -Ic $x > /dev/null
		then
			echo "$x $1" >> /etc/portage/package.use/$1;
		fi;
	 done
